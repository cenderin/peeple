class Contact
  include Mongoid::Document
  field :email, type: String
  field :phone_number, type: String
  field :first_name, type: String
  field :last_name, type: String
  field :user_id, type: Integer
  field :street, type: String
  field :street2, type: String
  field :city, type: String
  field :state, type: String
  field :zip_code, type: String
  field :contact_id, type: Integer

  belongs_to :user

# @b = Address.find_by(:contact_id => BSON::ObjectId('57964cc231c552473c000001'))

end
